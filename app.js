const express = require('express');
const articleRouter = require('./routes/articles')
const mongoose = require('mongoose')
const methodOverride = require('method-override')

const Article = require('./models/article')

mongoose.connect('mongodb://localhost/blog', {
    useNewUrlParser: true,
useUnifiedTopology: true,
    useCreateIndex: true
}, () => {
    console.log('MongoDB database connected')
})

// Initialise app
const app = express()

// Configure port
const port = process.env.PORT || 5000

// Configure view engine
app.set('view engine', 'ejs')

app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'));


// External router files
app.use('/articles', articleRouter);


// Define routes
app.get('/', async (req, res) => {
    const articles = await Article.find().sort({created_at: 'desc'});
    res.render('articles/index', {articles: articles});
});

app.listen(port, () => {
    console.log(`Server listening on port: ${port}`);
})